import numpy as np
from sz import im2col


def get_conv_outsize(_input_size, _kernel_size, _stride, _pad):
    """
    input_size：输入大小
    kernel_size： 卷积核（过滤器）大小
    stride： 步长
    pad： 填充
    """
    return (_input_size + _pad * 2 - _kernel_size) // _stride + 1


H, W = 4, 4
KH, KW = 3, 3
SH, SW = 1, 1
PH, PW = 1, 1

OH = get_conv_outsize(H, KH, SH, PH)
OW = get_conv_outsize(W, KW, SW, PW)
print(OH, OW)  # 4 4

x1 = np.random.rand(1, 3, 7, 7)
col1 = im2col(x1, kernel_size=5, stride=1, pad=0, to_matrix=True)
print(col1.shape)  # (9, 75)

x2 = np.random.rand(10, 3, 7, 7)
kernel_size = (5, 5)
stride = (1, 1)
pad = (0, 0)
col2 = im2col(x2, kernel_size, stride, pad, to_matrix=True)
print(col2.shape)  # (90, 75)
