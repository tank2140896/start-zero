import numpy as np

vector1 = np.array([[1, 2], [3, 4], [5, 6]]) # 3行2列。可以理解为有3组数据，每组数据有2个特征点
vector2 = np.array([[1, 2], [3, 4], [5, 6], [7, 8], [9, 0]]) # 5行2列。可以理解为有5组数据，每组数据有2个特征点

# 点积
dot_product = np.dot(vector1, vector2.T)
print(dot_product)

# 范数（这里采用的是向量的2范数，即向量的各元素的平方之和再开平方根）
norm_a = np.linalg.norm(vector1)
norm_b = np.linalg.norm(vector2)
print(norm_a, norm_b)

# 余弦相似度
cos_similarity = dot_product / (norm_a * norm_b) # 得出的是3组数据与5组数据的分别匹配度，值越大表示越相似
print(cos_similarity)

# ----------------------------------------------------------------------------------------------------

word1 = np.random.rand(1, 512)
word2 = np.random.rand(1, 512)

# 点积
dot_product = np.dot(word1, word2.T)
print(dot_product)

# 范数（这里采用的是向量的2范数，即向量的各元素的平方之和再开平方根）
norm_a = np.linalg.norm(word1)
norm_b = np.linalg.norm(word2)
print(norm_a, norm_b)

# 余弦相似度
cos_similarity = dot_product / (norm_a * norm_b)
print(cos_similarity)