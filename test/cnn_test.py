import os.path
import numpy as np
from PIL import Image

from sz import accuracy, MomentumSGD, VGG16, sigmoid_cross_entropy

# 字符串数字化（共62）
char_set = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V', 'W', 'X', 'Y', 'Z'
            ]


def digitizing_strings(_str):
    vector = np.zeros((len(_str), len(char_set)))  # 如：(4, 62)
    for i, c in enumerate(_str):
        index = char_set.index(c)
        vector[i][index] = 1.0
    vector = vector.reshape((1, len(_str) * len(char_set)))
    return vector


lr = 0.1  # 学习率
epoch = 10000  # 迭代次数
batch_size = 100  # 批量处理
pic_size = (94, 35)  # 图片像素
load_or_save_file = 'cnn_mlp.npz'  # 训练后的模型保存地址
train_file_path = 'E:\\ocr\\tain'  # 训练图片加载地址目录
test_file_path = 'E:\\ocr\\test'  # 测试图片加载地址目录


def test_one_pic(test_one_file_full_path):
    if os.path.exists(load_or_save_file):
        model = VGG16(classify_num=4 * 62)  # 加载模型VGG16
        if os.path.exists(load_or_save_file):
            model.load_parameters(load_or_save_file)
            image = Image.open(test_one_file_full_path)
            image = VGG16.preprocess(image)
            image = image[np.newaxis]  # (1, 3, 94, 35)
            x_grad = model(image)
            # print(x_grad)
            x_grad = x_grad.reshape((4, 62)).data
            for i in x_grad:
                print(char_set[np.argmax(i)])
            model.clear_tensors()


def train_pic():
    # 加载模型VGG16
    model = VGG16(classify_num=4 * 62)  # 加载模型VGG16
    # 使用优化器MomentumSGD并安装模型
    optimizer = MomentumSGD(lr).setup(model)

    file_list = []
    for file in os.listdir(train_file_path):
        file_path = os.path.join(train_file_path, file)
        if os.path.isfile(file_path):
            file_list.append(file_path)
    if len(file_list) > 0:
        for i in range(epoch):
            random_choice_index = np.random.choice(len(file_list), batch_size)
            chose_file_list = file_list[random_choice_index]
            concatenate_image_list = []
            concatenate_word_list = []
            for f in chose_file_list:
                word = digitizing_strings(f.split('_')[0])
                image = Image.open(f)
                image = VGG16.preprocess(image)
                image = image[np.newaxis]  # (1, 3, 94, 35)
                concatenate_image_list.append(image)
                concatenate_word_list.append(word)
            x = np.concatenate(concatenate_image_list, axis=0)
            t = np.concatenate(concatenate_word_list, axis=0)

            x_grad = model(x)
            # print(x_grad.shape, t.shape)
            loss = sigmoid_cross_entropy(t, x_grad)

            if loss.data <= 0:
                break

            model.clear_tensors()
            loss.backward()
            optimizer.update()

            if i % 100 == 0:
                # print(x_grad.shape, t.shape)
                accuracy_out = round(accuracy(x_grad, t) * 100, 2)
                print("准确率：" + str(accuracy_out) + "%，损失值：" + str(loss))

        model.save_parameters(load_or_save_file)

# train_pic()  # 训练
# test_one_pic()  # 测试
