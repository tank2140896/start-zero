import numpy as np

from sz import Tensor, dropout

dropout_ratio = 0.6
x = np.ones(10)
print(x)  # [1. 1. 1. 1. 1. 1. 1. 1. 1. 1.]
r = np.random.rand(10)
print(r)  # [0.85588054 0.07259442 0.35614602 0.51675783 0.66120252 0.82990559 0.08650816 0.73831981 0.79562727 0.30881343]
mask = r > dropout_ratio
print(mask)  # [True False False False True True False True True False]
y = x * mask  # [1. 0. 0. 0. 1. 1. 0. 1. 1. 0.]
print(y)
print('--------------------------------------------------')
a = 1
b = False
print(a * b)  # 0
print('--------------------------------------------------')
scale = 1 - dropout_ratio
r = np.random.rand(10)
print(r)  # [0.80465189 0.481967 0.15287762 0.30775312 0.95698669 0.64251153 0.11023048 0.73361371 0.94503785 0.99792246]
mask = r > dropout_ratio
print(mask)  # [True False False False True True False True True True]
y = x * mask / scale
print(y)
print('--------------------------------------------------')
t = Tensor(np.ones(10))
print(t)  # Tensor([1. 1. 1. 1. 1. 1. 1. 1. 1. 1.])
y = dropout(t, dropout_ratio=0.5)
print(y)  # Tensor([0. 2. 0. 2. 0. 0. 2. 2. 2. 0.])
