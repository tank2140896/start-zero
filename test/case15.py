import numpy as np

from sz import MLP, SGD, MomentumSGD, AdaGrad, AdaDelta, Adam
from sz import mean_squared_error

np.random.seed(0)
x = np.random.rand(100, 1)  # 100行1列
y = np.sin(2 * np.pi * x) + np.random.rand(100, 1)  # 100行1列

lr = 0.1
epoch = 100000

model = MLP((30, 40, 50, 40, 30, 1))
optimizer = MomentumSGD(lr).setup(model)

for i in range(epoch):
    y_grad = model(x)
    loss = mean_squared_error(y, y_grad)
    model.clear_tensors()
    loss.backward()
    optimizer.update()
    if i % 10000 == 0:
        print(loss)
"""
Tensor(0.6983994697470247)
Tensor(0.07147807999363116)
Tensor(0.06686731420676507)
Tensor(0.06457327696768168)
Tensor(0.06346820184233609)
Tensor(0.06303657281191642)
Tensor(0.06303644371767608)
Tensor(0.058649099331826014)
Tensor(0.05480300101330009)
Tensor(0.054394965842818575)
"""
