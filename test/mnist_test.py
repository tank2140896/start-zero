import os.path
import numpy as np
from sz import mean_squared_error, accuracy, MLP, MomentumSGD, relu
from mnist import load_mnist


def train():
    lr = 0.1  # 学习率
    epoch = 10000  # 迭代次数
    batch_size = 100  # 批量处理
    load_or_save_file = 'mnist_mlp.npz'

    # 加载模型MLP（多层感知器）并使用线性整流激活函数（relu）
    model = MLP((200, 100, 10), activation=relu)
    # 使用优化器MomentumSGD并安装模型
    optimizer = MomentumSGD(lr).setup(model)

    if os.path.exists(load_or_save_file):
        model.load_parameters(load_or_save_file)

    (x_train, t_train), (x_test, t_test) = load_mnist(normalize=True, flatten=True, one_hot_label=True)  # 加载训练集
    for i in range(epoch):
        # 从0-len(x_train)间随机选择batch_size个数字（可能重复）
        random_choice_index = np.random.choice(len(x_train), batch_size)
        x = x_train[random_choice_index]  # 100行784列
        t = t_train[random_choice_index]  # 100行784列

        if os.path.exists(load_or_save_file):
            x_grad = model(x)
            loss = mean_squared_error(t, x_grad)
            model.clear_tensors()
            if i % 1000 == 0:
                accuracy_out = round(accuracy(x_grad, t) * 100, 2)
                print("准确率：" + str(accuracy_out) + "%，损失值：" + str(loss))
        else:
            x_grad = model(x)
            loss = mean_squared_error(t, x_grad)
            model.clear_tensors()

            loss.backward()
            optimizer.update()
            if i % 1000 == 0:
                accuracy_out = round(accuracy(x_grad, t) * 100, 2)
                print("准确率：" + str(accuracy_out) + "%，损失值：" + str(loss))

    model.save_parameters(load_or_save_file)


train()
"""
(x_train, t_train), (x_test, t_test) = load_mnist(normalize=True, flatten=True, one_hot_label=True)  # 加载训练集
x_one, t_one = x_train[0], t_train[0]
plt.imshow(x_one.reshape(28, 28), cmap='gray')
plt.axis('off')
plt.show()
for e in range(0, len(t_one)):
    if t_one[e] == 1:
        print(e)
        break
"""
