import numpy as np

from sz import Tensor, to_tensor

t = Tensor(np.random.randn(3, 2, 4))
print(len(t))  # 3
print(t)
print(t.shape)  # (3, 2, 4)
print(t.size)  # 24
print(t.ndim)  # 3
print(t.dtype)  # float64
print(t.T)
print(t.transpose())
print(t.transpose((1, 0, 2)))
print(t.reshape((12, 1, 2)))
print(to_tensor(10))  # Tensor([10])
print(to_tensor(np.array(10)))  # Tensor([10])
print(to_tensor(np.array([10])))  # Tensor([10])
print(to_tensor(10 + 1))  # Tensor([11])
print(to_tensor(np.array(10) + 1))  # Tensor([11])
print(to_tensor(np.array([10]) + 1))  # Tensor([11])

x = 10
y = Tensor(np.array(1.))
z = np.array([2, 3])

print(x + y + z)  # Tensor([13. 14.])

a = Tensor(np.array(2.))
b = -2
print(a**b)  # Tensor([0.25])

a = Tensor(-5)
b = -3
print(a**b)  # Tensor([-0.008])
