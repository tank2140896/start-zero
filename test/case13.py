import numpy as np

from sz import Tensor

x = Tensor(np.array([2]))
y = (x ** 3 + x) * 5
print(y)  # Tensor([50])
y.backward()
print(x.grad)  # Tensor([65])
z = x.grad
x.clear_tensor()
z.backward()
print(x.grad)  # Tensor([60])
z = x.grad
x.clear_tensor()
z.backward()
print(x.grad)  # Tensor([30])
z = x.grad
x.clear_tensor()
z.backward()
print(x.grad)  # Tensor([0.])
z = x.grad
x.clear_tensor()
z.backward()
print(x.grad)  # Tensor([-0.])
z = x.grad
x.clear_tensor()
z.backward()
print(x.grad)  # Tensor([0.])
