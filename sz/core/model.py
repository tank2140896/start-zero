from sz.core.layer import Layer


class Model(Layer):
    """
    所有模型的父类（基类）
    model模型可以理解为是对layer（层）的再次抽象、封装
    """
    pass
