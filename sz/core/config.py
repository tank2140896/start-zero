class Config:

    ENABLE_BACKPROP = True  # 是否能够反向传播

    ENABLE_GPU = False  # 是否使用GPU

    TRAIN = True  # 是否训练
